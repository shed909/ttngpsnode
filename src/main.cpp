#include <Arduino.h>
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <CayenneLPP.h>      // Library for encoding payloads
#include <U8x8lib.h>         // Library for drawing display when charged
#include <TinyGPS++.h>       // Library for reading GPS
#include <ttn_creds.h>       // TTN creds from include/ttn_creds.h

/*******************************************************************************/
/*                               Global options                                */
/*******************************************************************************/
#define BAT_PIN 35              /* Pin for battery voltage reading       */
#define DISP_PIN 16             /* Pin for display                       */
#define MAX_CLOCK_ERROR_PCT 1   /* Relax the LMIC clock error percentage */

// Start: Lora pin mapping for TTGO Lora32 V2.1_1.6
#define PIN_LMIC_NSS 18
#define PIN_LMIC_RXTX LMIC_UNUSED_PIN
#define PIN_LMIC_RST 23
#define PIN_LMIC_DIO0 26
#define PIN_LMIC_DIO1 33
#define PIN_LMIC_DIO2 32
// End: Lora pin mapping

//Start: GPS module pin mapping
#define PIN_GPS_RX 15
//End: GPS module pin mapping

const unsigned TX_INTERVAL = 600; // Schedule TX every this many seconds
/*******************************************************************************/

/*******************************************************************************/
/*                              Global Runtime                                 */
/*******************************************************************************/
CayenneLPP lpp(51);             /* Set payload size for CeyenneLPP */
#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro-sec->sec */
bool sleepReq = false;          /* Set to true to request board to sleep */
bool charging = false;          /* Set to true when charging */
float batLevel = 0.0;           /* Keep track of the battery level */
float lat = 0.0;
float lng = 0.0;                /* Keep track of lat, lng, alt */
float alt = 0.0;
U8X8_SSD1306_128X64_NONAME_HW_I2C display(/*rst*/ U8X8_PIN_NONE); /* Display */
RTC_DATA_ATTR lmic_t RTC_LMIC;  /* Save LMIC struct during sleep */
unsigned long lastDispCharged;   /* Keep track of when charging last displayed */
unsigned long lastSend;         /* Keep track of when last TX sent */
/*******************************************************************************/

/*******************************************************************************/
/*                                  Setup LMIC                                 */
/*******************************************************************************/
// START: Define TTN keys
// See include/example.ttn_creds.h for configuring the below EUI's
static const u1_t PROGMEM APPEUI[8]= TTN_APPEUI;
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

static const u1_t PROGMEM DEVEUI[8]= TTN_DEVEUI;
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

static const u1_t PROGMEM APPKEY[16]= TTN_APPKEY;
// EMD: Define TTN keys

// Get the DevKey from device
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

// Pin mapping per defined above
const lmic_pinmap lmic_pins = {
    .nss = PIN_LMIC_NSS,
    .rxtx = PIN_LMIC_RXTX,
    .rst = PIN_LMIC_RST,
    .dio = { PIN_LMIC_DIO0, PIN_LMIC_DIO1, PIN_LMIC_DIO2 },
};
/*******************************************************************************/

/*******************************************************************************/
/*                                  Setup GPS                                  */
/*******************************************************************************/
TinyGPSPlus gps;                                    /* Create GPS object */
HardwareSerial gpsSerial(2);                        /* Create GPS serial */
const unsigned gpsBaud = 9600;                      /* Set GPS serial baud */
/*******************************************************************************/

/*******************************************************************************/
/*                               Declare functions                             */
/*******************************************************************************/
void do_send(osjob_t* j);
void get_bat(int nbMeasurements);
void get_gps();

// Init osjob_t for sendjob and dispchargedjob
static osjob_t sendjob;
static osjob_t dispchargedjob;
static osjob_t dosleepjob;
/*******************************************************************************/

void printHex2(unsigned v) {
    v &= 0xff;
    if (v < 16)
        Serial.print('0');
    Serial.print(v, HEX);
}

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("AppSKey: ");
              for (size_t i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                printHex2(artKey[i]);
              }
              Serial.println("");
              Serial.print("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      printHex2(nwkKey[i]);
              }
              Serial.println();
            }
            // Disable link check validation
            LMIC_setLinkCheckMode(0);
            break;
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.print(F("Received "));
              Serial.print(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
            sleepReq = true; /* Sleep requested */
            // Schedule transmission, limited to every TX_INTERVAL cycle
            if (millis() - lastSend >= (TX_INTERVAL * uS_TO_S_FACTOR)) {
              do_send(&sendjob);
              lastSend = millis();
            }
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            Serial.println(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            break;

        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void do_send(osjob_t* j) {
    // Update GPS
    get_gps();
    // Update batery voltage
    get_bat(5);
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    }
    else {
      lpp.addAnalogInput(1, batLevel); // Add battery volts to lpp buffer
      lpp.addGPS(1, lat, lng, alt); // Add GPS lat, lng, alt to lpp buffer
      // Prepare upstream data transmission at the next possible time.
      LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
      lpp.reset();
      Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

// From example here:
// https://github.com/JackGruber/ESP32-LMIC-DeepSleep-example
void save_to_rtc(int /* use TX_INTERVAL */ sleepInterval) {

    // Save to RTC_LMIC we defined earlier
    Serial.println(F("Saving to RTC"));
    RTC_LMIC = LMIC;

    // ESP32 can't track millis during DeepSleep and
    // no option to advanced millis after DeepSleep.
    // Therefore, reset DutyCyles
    Serial.println(F("Resetting LMIC duty avail"));
    RTC_LMIC.globalDutyAvail = LMIC.globalDutyAvail
                              - ((millis() / 1000.0 + sleepInterval)
                              * OSTICKS_PER_SEC);
    if (RTC_LMIC.globalDutyAvail < 0)
    {
       RTC_LMIC.globalDutyAvail = 0;
    }
}

// From example here:
// https://github.com/JackGruber/ESP32-LMIC-DeepSleep-example/blob/master/src/main.cpp
// https://jackgruber.github.io/2020-04-13-ESP32-DeepSleep-and-LoraWAN-OTAA-join
void load_from_rtc() {
    Serial.println(F("Loading from RTC"));
    LMIC = RTC_LMIC;
    LMIC.opmode = 0x000; // Reset opmode, seems save as OP_TXRXPEND OP_NEXTCHNL
}

void do_sleep(osjob_t* j) {
    // Save to RTC
    save_to_rtc(TX_INTERVAL);
    Serial.println(F("Going to sleep!"));
    // Flush serial - waits for any outstading buffer
    Serial.flush();
    // Set sleepRq to false
    sleepReq = false;
    // Go to sleep
    esp_deep_sleep_start();
}

void get_bat(int nbMeasurements) {
    // Take x measurements
    int value = 0;
    for (int i = 0; i < nbMeasurements; i++) {
        value += analogRead(BAT_PIN);
        delay(10);
    }
    // Calculate actual voltage by averaging all readings
    batLevel = ((value/nbMeasurements) / 4095.0) * 7.26;
}

// From example here:
// https://gist.github.com/jenschr/dfc765cb9404beb6333a8ea30d2e78a1
// TODO: Find a better way to indicate charing.
//       The voltage doesn't read 4.3 when on USB until it's halfway charged.
char batString[25];
char voltString[25];
void display_charged(osjob_t* j) {
    if (batLevel > 4.3) {
      // Enable display pin
      digitalWrite(DISP_PIN, HIGH);
      // Display "Charged..."
      display.setFont(u8x8_font_victoriamedium8_r);
      display.drawString(0, 0, "Charged...");
      // Display voltage level
      itoa(batLevel,batString,10);
      sprintf(batString,"%7.2f",batLevel);
      display.drawString(2, 4, batString);
      // Display "V" unit
      sprintf(voltString,"%s","V");
      display.drawString(9, 4, voltString);
      // Disable display pin, save power
      digitalWrite(DISP_PIN, LOW);
    }
    else {
      // Clear display if not charging
      display.clear();
    }
}

// From documentation here:
// http://arduiniana.org/libraries/tinygpsplus/
void get_gps() {
    // Read data from GPS for 1 second - hopefully we get data
    for (unsigned long start = millis(); millis() - start < 1000;) {
      while (gpsSerial.available() > 0)
        gps.encode(gpsSerial.read());
    }

    // If GPS updated and valid, update lat, lng, alt
    if (gps.location.isUpdated() && gps.location.isValid()) {
      lat = gps.location.lat();
      Serial.print("LAT="); Serial.print(lat, 6);
      lng = gps.location.lng();
      Serial.print("LNG="); Serial.println(lng, 6);
      alt = gps.altitude.meters();
      Serial.print("ALT="); Serial.println(alt, 6);
      gpsSerial.flush(false);
    }
    // If no valid data, print error
    if (millis() > 5000 && gps.charsProcessed() < 10) {
      Serial.println(F("ERROR: no GPS data!"));
    }
}

void setup() {
    Serial.begin(9600);
    gpsSerial.begin(gpsBaud, SERIAL_8N1, PIN_GPS_RX); // Serial for GPS reading

    pinMode(BAT_PIN, INPUT);     // Set battery pin to input
    pinMode(DISP_PIN, OUTPUT);  // Set display pin to output

    // // Get initital battery reading
    get_bat(2);

    // Initialize display
    display.begin();

    // Let LMIC compensate for +/- % clock error
    LMIC_setClockError(MAX_CLOCK_ERROR * MAX_CLOCK_ERROR_PCT / 100);

    // LMIC init
    os_init();

    // Reset the MAC state
    LMIC_reset();

    if (RTC_LMIC.seqnoUp > 0) {
      load_from_rtc();
    }

    // Select subband 2 - used by TTN for AU915
    LMIC_selectSubBand(1);

    // Set timer from TX_INTERVAL value, multiply by uS_TO_S_FACTOR
    esp_sleep_enable_timer_wakeup(TX_INTERVAL * uS_TO_S_FACTOR);

    // Start sendjob (starts OTAA)
    do_send(&sendjob);
}

void loop() {
      // Run LMIC
      if (lat + lng + alt > 0) {
        os_runloop_once();
      }
      else {
        Serial.println(F("No GPS data, not sending"));
        sleepReq = true;
      }

      // Schedule "charged" display, limited to every 2000ms / 2sec
      if (millis() - lastDispCharged >= 2000) {
        display_charged(&dispchargedjob);
        lastDispCharged = millis();
      }

      // Check for time critical jobs
      const bool timeCriticalJobs = os_queryTimeCriticalJobs(
                                    ms2osticksRound((TX_INTERVAL * 1000)
                                                    - 1000));
      // Sleep if requested and no time critical jobs
      if ( !timeCriticalJobs && sleepReq ) {
        do_sleep(&dosleepjob); // Run function to sleep
      }
}
