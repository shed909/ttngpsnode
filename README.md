# Introduction

A GPS Node utilising LoraWAN to log GPS data over The Things Network V3
community network.

# Configuration

### TTN

The TTN EUI's are configured in a header file, which can be placed in:
`include/ttn_creds.h`

See `include/example.ttn_creds.h` for more info.

### Pin Mapping

The pin mapping is configured at the top of `main.cpp`, where the battery and
display pins can be changed to suit the board. The LMIC pin mapping is configured
here, as well as GPS RX pin.

[This](https://www.thethingsnetwork.org/forum/t/big-esp32-sx127x-topic-part-3/18436)
thread is useful for finding pin mappings for different boards.

The TX_INTERVAL can also be changed. Default is 60 seconds, but this can vary slightly
depending on LMIC's job queue.

### Payload Decoder

This node uses CeyenneLPP library to encode the payload, as it makes everything really
easy.
This can be configured at the app level for all end device, or at the end device
level.

Via the app:

Applications > your-application > Payload Formatters > Uplink

Via the device:

End Devices > your-device > Payload Formatters > Uplink
